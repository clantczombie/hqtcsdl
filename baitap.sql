﻿use master
Drop database QLBANHANG
CREATE DATABASE QLBANHANG
On primary
(
	Name=QLBanHang_data,
	FileName='D:\New Folder\QL_Ban_Hang.mdf',
	Size=30MB,
	MaxSize=150MB,
	FileGrowth=10%
)
Log On
(
	Name=QLSV_log,
	FileName='D:\New Folder\QLSV.ldf',
	Size=10MB,
	MaxSize=unlimited,
	FileGrowth=3MB
);
Use QLBANHANG

CREATE TABLE NHANVIEN
(
	MSNV CHAR(6) NOT NULL PRIMARY KEY,
	TENNV NVARCHAR(50) NOT NULL,
	NGAYSINH DATETIME NOT NULL,
	PHAI NVARCHAR(5) NOT NULL,
	DIACHI NVARCHAR(100) NOT NULL,
	DIENTHOAI VARCHAR(15) ,
	
)
CREATE TABLE KHACHHANG
(
	MSKH INT identity(1,1) NOT NULL PRIMARY KEY,
	TENKH NVARCHAR(50) NOT NULL,
	PHAI NVARCHAR(5) ,
	DIACHI NVARCHAR(100),
	DIENTHOAI VARCHAR(15),
	
)
CREATE TABLE MATHANG
(
	MSMH CHAR(6) NOT NULL PRIMARY KEY,
	TENKH NVARCHAR(50) NOT NULL,
	SL_TON INT,
	DONGIA INT,
	DONVITINH NVARCHAR(20)
)
CREATE TABLE HOADON
(
	MSHD INT identity(1,1) NOT NULL PRIMARY KEY,
	MSNV CHAR(6) NOT NULL,
	MSKH INT NOT NULL,
	NGAYHD DATETIME DEFAULT GETDATE(),
	TONGTIEN MONEY ,
	CONSTRAINT FK_MSNV FOREIGN KEY (MSNV) REFERENCES NHANVIEN(MSNV)
)

CREATE TABLE CHITIET_HD
(
	MSHD INT  NOT NULL ,
	MSMH CHAR(6) NOT NULL ,
	SOLUONG INT ,
	THANHTIEN MONEY ,
	CONSTRAINT FK_CHITIET_HD PRIMARY KEY(MSHD,MSMH),
	CONSTRAINT FK_MSHD FOREIGN KEY(MSHD) REFERENCES HOADON(MSHD),
	CONSTRAINT FK_MSMH FOREIGN KEY(MSMH) REFERENCES MATHANG(MSMH),
)
insert into KHACHHANG values (N'Nguyễn Thái Hòa',N'Nam',N'305 Đại lộ 3','')
insert into KHACHHANG values (N'Nguyễn Kiên Viễn',N'Nam',N'30 Vườn Chuối','')
insert into KHACHHANG values (N'Phạm Ngọc Lan',N'Nữ',N'11 Bùi Thị Xuân','0933124456')
insert into KHACHHANG values (N'Nguyễn Ngọc Anh Thư',N'Nữ','HCM','')
insert into KHACHHANG values (N'Nguyễn Văn A',N'Nam','215 Điện Biên Phủ','0918123123')

--Nhanvien
insert into NHANVIEN values ('NV001',N'Nguyễn Văn Bi','1979-07-15',N'Nam',N'16/11 Trần Hưng Đạo Q.1','0918299583')
insert into NHANVIEN values ('NV002',N'Nguyễn Thị Na','1980-09-22',N'Nữ',N'250 Tô Hiệu','0903923370')
insert into NHANVIEN values ('NV003',N'Nguyễn Văn Bin','1979-06-12',N'Nam',N'16 Tô Hiến Thành','')
insert into NHANVIEN values ('NV004',N'Trần Văn Anh','1980-06-12',N'Nam',N'161 Gò Xoài','')
insert into NHANVIEN values ('NV005',N'Trần Thùy Trinh','1981-06-02',N'Nữ',N'Tây Ninh','')
insert into NHANVIEN values ('NV006',N'Nguyễn Thị Kim Chi','1980-06-22',N'Nữ',N'Tiền Giang','')
--Mat Hàng
insert into MATHANG values('C0001',N'CPU i5 4x2.5 GHz','49','2000000',N'Chiếc')
insert into MATHANG values('C0002',N'CPU i3 4x2.0 GHz','49','1500000',N'Chiếc')
insert into MATHANG values('C0003',N'CPU i7 8x1.8 GHz','49','2000000',N'Chiếc')
insert into MATHANG values('K0001',N'Keyboard P/s 2','99','80000',N'Chiếc')
insert into MATHANG values('L0001',N'Nguồn 400KW','10','300000',N'Chiếc')
insert into MATHANG values('M0001',N'Mouse HP p/s 2','0','56000',N'Chiếc')
insert into MATHANG values('M0002',N'Mouse HP USP','99','120000',N'Chiếc')
--Hóa Đơn
insert into HOADON values('NV001','2','2000-03-26','2150000')
insert into HOADON values('NV003','3','2004-03-27','2150000')
insert into HOADON values('NV002','4','2013-03-05','8430000')
--ChiTiet_HD
insert into CHITIET_HD values('1','C0001','1','2000000')
insert into CHITIET_HD values('1','K0001','1','80000')
insert into CHITIET_HD values('1','M0001','1','70000')
insert into CHITIET_HD values('2','C0003','1','2000000')
insert into CHITIET_HD values('2','M0002','1','150000')
insert into CHITIET_HD values('3','C0002','1','1500000')
insert into CHITIET_HD values('3','M0001','99','6930000')
EXEC sp_rename 'mathang.tenkh', 'TenMH', 'COLUMN';
--Bài 4
--Câu 1)
alter table NHANVIEN ADD SoLan_GiaoDich int;
go
--Câu 2)
alter table CHITIET_HD add GiaBan money;
go
--Câu 4)
alter table NHANVIEN add constraint SoLan_GD default('0') for SoLan_GiaoDich;
go
--Câu 5)
alter table HOADON add constraint NGAY_HD default(getdate()) for NGAYHD;
go
--Câu 6)
alter table chitiet_hd add check (soluong>0)
go
--Câu 7)
alter table nhanvien add constraint rule_ngaysinh check(ngaysinh < getdate())
go
--Câu 8)  
alter table mathang add unique(TenMH)
go
--Câu 9)
alter table hoadon drop constraint DF__HOADON__NGAYHD__0CBAE877
alter table chitiet_hd drop constraint CK__CHITIET_H__SOLUO__1367E606
alter table nhanvien drop constraint rule_ngaysinh
alter table mathang drop constraint UQ__MATHANG__4CF9ACB5164452B1
--Câu 10)
select * from MATHANG
select @@ROWCOUNT
--Câu 11)
select @@SERVERNAME
--Câu 12)
CREATE INDEX i1 ON nhanvien (dienthoai); 
CREATE INDEX i2 ON khachhang (dienthoai); 
--Câu 13)
DROP INDEX i2 ON khachhang;
--Câu 6.1)
select CHITIET_HD.* from CHITIET_HD,hoadon 
where CHITIET_HD.MSHD = HOADON.MSHD and hoadon.NGAYHD = '2013-03-05'
--Câu 6.2)
select KHACHHANG.MSKH,KHACHHANG.TENKH,KHACHHANG.DIENTHOAI from KHACHHANG,HOADON 
where KHACHHANG.MSKH = HOADON.MSKH 
--Câu 6.3)
select MATHANG.* from MATHANG,CHITIET_HD 
where MATHANG.MSMH != CHITIET_HD.MSMH
--Câu 6.4)
select * from MATHANG 
where MATHANG.MSMH = (select ct.MSMH
						from MATHANG as mh,CHITIET_HD as ct 
						where mh.MSMH=ct.MSMH 
						group by ct.MSMH
						having COUNT(ct.MSHD) = ( select max(ss)
													from (	select count(ct.MSHD) ss
														from MATHANG as mh,CHITIET_HD as ct 
														where mh.MSMH=ct.MSMH 
														group by ct.MSMH
													 ) As A
											))

--Câu 6.5)
select * from HOADON where HOADON.TONGTIEN > 5000000
--Câu 6.6)
select * from HOADON as hd where hd.TONGTIEN in (select MAX(HOADON.TONGTIEN) from HOADON)
--Câu 6.7)
select COUNT(kh.MSKH)  as 'So lan mua' 
from HOADON as hd,KHACHHANG as kh 
where hd.MSKH = kh.MSKH and kh.MSKH = 101 
group by hd.MSKH
--
--7.1)Cho biết doanh số bán của từng mặt hàng trong 6 tháng đầu năm 2011
select m.msmh MSMH,m.tenkh TenMH
from chitiet_hd c , mathang m,hoadon hd
where c.msmh = m.msmh and hd.mshd = c.mshd and hd.ngayhd between '2011-01-01' and '2011-06-30'
group by m.msmh,m.tenkh
--7.2)	Cho biết mặt hàng có doanh bán xuất lớn nhất. 
select m.msmh,m.tenkh
from chitiet_hd c , mathang m
where c.msmh = m.msmh
group by m.msmh,m.tenkh
having sum(c.thanhtien) = (
							select max(MaxDS)
							from (
									select sum(ct.thanhtien) as MaxDS
									from chitiet_hd as ct 
									group by ct.msmh
								) as a
							)
--7.3)Cho biết mặt hàng có tổng doanh thu lớn hơn 20.000.000. 
select m.msmh,m.tenkh, sum(c.thanhtien)
from chitiet_hd c,mathang m
where c.msmh = m.msmh
group by m.msmh,m.tenkh 
having sum(c.thanhtien) > 20000000
--7.4)Cho biết doanh thu của từng mặt hàng theo từng tháng. 
select m.msmh,m.tenkh,month(hd.ngayhd),sum(c.thanhtien)
from chitiet_hd c,mathang m,hoadon hd
where c.msmh = m.msmh and hd.mshd = c.mshd
group by m.msmh,m.tenkh ,month(hd.ngayhd)
--7.5)	Cho biết mặt hàng có doanh bán xuất lớn nhất. 


from chitiet_hd c , mathang m,hoadon hd
where c.msmh = m.msmh and hd.mshd = c.mshd and month(hd.ngayhd) = 11
group by m.msmh,m.tenkh 
having sum(c.thanhtien) = (
							select max(MaxDS)
							from (
									select sum(ct.thanhtien) as MaxDS
									from chitiet_hd as ct 
									group by ct.msmh
								) as a
							)
--7.6)Cho biết những khách hàng có tổng tiền mua hàng trên 10 triệu và địa chỉ ở TP.HCM.
create view "KH" as
select kh.mskh,kh.tenkh
from khachhang kh,hoadon hd
where kh.mskh = hd.mskh and hd.tongtien > 10000000 and kh.diachi = 'HCM'
group by kh.mskh,kh.tenkh
sp_helptext @objname = KH
--7.7)Cho biết danh sách mặt hàng có đơn giá từ 2 triệu đến 20 triệu. Có sử dụng WITH CHECK OPTION. Sau đó chèn thử một ản ghi vào view này. 
create view mh_2_20 as
select * 
from mathang
where dongia between 2000000 and 20000000 WITH CHECK OPTION;

insert into mh_2_20 values('MK1',N'Keyboard P/s 2','99','2000000',N'Chiếc')\
------------------------------------------------
/*
8.Cursor.
1.Viêt một đoạn lệnh sử dụng con trỏ để hiển thị danh sách khách hàng. Nếu khách hàng
đã mua hàng với tổng tiền trên 2triệt thì thông báo được khuyến mãi 5%, trên 10triệu
là 10%
*/
Declare CurKH Cursor
For Select MSKH,TENKH from KHACHHANG
Open CurKH
Declare @makh int,@tenkh nvarchar(50),@TongTien int
Fetch next FROM CurKH Into @makh,@tenkh
while @@fetch_status = 0
Begin
Select @TongTien = SUM(TONGTIEN) from HOADON
where MSKH = @makh
IF @TongTien >= 10000000
Print 'MSKH ' + CONVERT(varchar(10),@makh) + ' Ten KH: ' + @tenkh +'Tong Tien: '+CONVERT(varchar(10),@TongTien)+' Khuyen Mai 10%'
ELSE IF @TongTien >= 2000000 AND @TongTien < 10000000
Print 'MSKH ' + CONVERT(varchar(10),@makh) + ' Ten KH: ' + @tenkh +'Tong Tien: '+CONVERT(varchar(10),@TongTien*0.95)+' Khuyen Mai 5%'
ELSE
Print 'MSKH ' + CONVERT(varchar(10),@makh) + ' Ten KH: ' + @tenkh +'Tong Tien: '+CONVERT(varchar(10),@TongTien)+' Khuyen Mai 0%'
fetch next from curKH into @makh,@tenkh
End
Close CurKH
Deallocate CurKH
/*2.Thêm một thuộc tính trong So_Lan_GiaoDich kiểu dữ liệu số nguyên trong bảng
NhanVien để lưu số lần giao dịch (bán hàng) của từng nhân viên. Viết cursor cập nhật
cho thuộc tính này.*/
Declare Curnhanvien Cursor
For Select MSNV FROM NHANVIEN
OPEN Curnhanvien
Declare @msnv char(6)
Fetch next from Curnhanvien into @msnv
while @@fetch_status = 0
Begin 
Declare @SoLanGD int
Select @SoLanGD = COUNT(*) FROM HOADON
WHERE MSNV = @msnv
UPDATE NHANVIEN
SET SoLan_GiaoDich = @SoLanGD
WHERE MSNV = @msnv
Fetch next from Curnhanvien into @msnv
End
Close Curnhanvien
Deallocate Curnhanvien
/*3.Thêm thuộc tính TongMH trong bang HoaDon lưu thông tin tổng số mặt hàng đã mua
trong đơn hàng. Viết cursor cập nhật thông tin cho thuộc tính này */
go
ALTER TABLE HOADON
ADD TongMH int
go
Declare CurHoaDon Cursor
FOR SELECT MSHD FROM HOADON
Open CurHoaDon
Declare @mshd int
Fetch next from CurHoaDon into @mshd
while @@fetch_status = 0
Begin
Declare @TongSMatHang int
Select @TongSMatHang = COUNT(MSMH)
FROM HOADON H,CHITIET_HD C
WHERE H.MSHD = C.MSHD AND H.MSHD = @mshd
UPDATE HOADON
SET TongMH = @TongSMatHang
WHERE MSHD = @mshd
Fetch next from CurHoaDon Into @mshd
End
Close CurHoaDon
Deallocate CurHoaDon
/*
SELECT COUNT(*) FROM HOADON
WHERE MSNV = 'NV001'
*/
/********/
/*Cau 4.Thêm thuộc tính TongTien (nếu chưa có) trong bảng HoaDon lưu thông tin tổng thành
tiền trong hóa đơn hàng. Viết cursor cập nhật thông tin cho thuộc tính này. */
Declare Curcapnhathd Cursor
For Select MSHD FROM HOADON
Open Curcapnhathd
Declare @mshd int
Fetch next from Curcapnhathd Into @mshd
while @@fetch_status = 0
Begin
Declare @TongTien int
Select @TongTien = SUM(THANHTIEN) FROM CHITIET_HD
WHERE MSHD = @mshd
UPDATE HOADON
SET TONGTIEN = @TongTien
WHERE MSHD = @mshd
Fetch next from Curcapnhathd into @mshd
End
Close Curcapnhathd
Deallocate Curcapnhathd
/**/
SELECT * FROM CHITIET_HD
SELECT * FROM HOADON
SELECT * FROM MATHANG
SELECT * FROM NHANVIEN
SELECT * FROM KHACHHANG
--
/*10.1)Nhập vào tháng và năm bất kỳ cho biết thông tin 
những mặt hàng bán trong tháng đó. */
create PROCEDURE getInfoProductMonth(@ye int,@mon int)
as
begin
	select mh.*
	from hoadon hd,chitiet_hd ct,mathang mh
	where hd.mshd=ct.mshd and mh.msmh=ct.msmh
	and year(hd.ngayhd) = @ye and month(hd.ngayhd) = @mon
end
/*10.2)Nhập vào một quý bất kỳ cho biết
những mặt hàng nào bán nhiều nhất trong quý đó*/
create PROCEDURE getInfoProductPart(@part int)
as
begin
	select top 1 mh.*
	from hoadon hd,chitiet_hd ct,mathang mh
	where DATEPART(q,hd.ngayhd) = @part and 
	mh.msmh = (select ct2.msmh
				from chitiet_hd ct2 
				group by ct2.msmh
				having sum(ct2.soluong) = (select max(maxSL) as topSL
											 from (
											select msmh,sum(soluong) maxSL
											from chitiet_hd
											group by msmh) as a))
				
end
exec getInfoProductPart 1
/*10.3)Nhập vào MSHD cho biết thông tin 
những mặt hàng của những hóa đơn ngày*/
create PROCEDURE getInfoProductDays(@mshd int)
as
begin
	select b1.*
	from mathang b1
	where b1.msmh in (select mh.msmh
					from hoadon hd,mathang mh,chitiet_hd ct
					where mh.msmh = ct.msmh and 
					ct.mshd = (
								select hoadon.mshd MSHD
								from hoadon
								where hoadon.mshd = @mshd 
								)
					group by mh.msmh)
end
exec getInfoProductDays 1
--
/*10.4)*/
go
create PROCEDURE insertMatHang 
(	@msmh	char(6), 
	@tenMH	nvarchar(50),
	@sl_ton int,
	@dongia int,
	@donvitinh nvarchar(20)
)as
begin
	--Kiem tra khoa chinh
	if(not exists(select msmh from mathang where msmh = @msmh) 
		and @donvitinh in (N'Cái', N'Bộ', N'Thùng', N'Hộp') 
		and @sl_ton > 0 
		and @dongia > 0)
			insert into mathang(msmh,tenmh,sl_ton,dongia,donvitinh) 
			values (@msmh,@tenMH,@sl_ton,@dongia,@donvitinh)
	else 
		Print 'An error, contact Admin.Please!'
	
end
go
exec insertMatHang 'M000zx',N'Mouse HP USP','99','120000',N'Chiếc'
/*10.5*/
create procedure insertChiTietHD
(
	@mshd int,
	@msmh char(6),
	@soluong int,
	@thanhtien int
)
as
begin
	if (not exists (select * from chitiet_hd where msmh = @msmh and mshd = @mshd)
		and exists(select mshd from hoadon where mshd = @mshd) 
		and exists(select msmh from mathang where msmh = @msmh) 
		and @soluong > 0 and @soluong <= all (
										select sl_ton
										from mathang mh
										where mh.msmh = @msmh
											)
		)
		insert into CHITIET_HD values(@mshd,@msmh,@soluong,@thanhtien)
	else 
		print 'An error!'
end
exec insertChiTietHD '1','C0003','1','2000000'


/*10.1B)Đầu vào: MaMH, ngày 1, ngày 2. Đầu ra: Tổng số lượng
hàng đã bán của mặt hàng đó
trong khoảng từ ngày 1 đến ngày 2*/
alter proc getTotalQuantityProduct
(
	@msmh char(6),
	@date1 datetime,
	@date2 datetime
)
as
begin
	select sum(ct.soluong) 
	from hoadon hd,chitiet_hd ct
	where ct.mshd in 
					(select mshd from hoadon 
					where ngayhd  between @date1 and @date2)
		and ct.msmh = @msmh
end

exec getTotalQuantityProduct 'M0001','2000-03-01','2000-03-26'
/*10.2B)Đầu vào: TenKH, kiểm tra khách hàng đó có tồn tại không. Đầu ra: tổng số lượng mặt
hàng mà khách hàng đó đã mua.
*/
alter proc getTotalQuantityProductCustomer
(
	@tenkh nvarchar(50)
)
as
begin
	if (exists(select * from khachhang where tenkh like @tenkh))
		select sum(ct.soluong)
		from hoadon hd,chitiet_hd ct
		where hd.mshd=ct.mshd and mskh in (
										select mskh 
										from khachhang
										where tenkh like @tenkh
										)
	else 
		print 'Khong co KH'

end

exec getTotalQuantityProductCustomer 'Nguyễn Thái Hòa'


/*10.3B)Đầu vào MAKH. Kiểm tra khách hàng đó có tồn tại không. Đầu ra, tổng số lần mua
hàng và tổng trị giá trên các lần đặt hàng của khách hàng. */
create proc TongGiaTriMuaHang(@mskh int)
as
begin
	select count(*),sum(tongtien)
	from hoadon
	where hoadon.mskh = @mskh
end
/*10.4B)Đầu vào: MaMH. Đầu ra: Số lượng tồn của mặt hàng đó*/
create proc SoLuongTon(@msmh char(6))
as
begin
	select sl_ton from mathang where msmh = @msmh
end
/*10.5B)Đầu vào: Tháng và năm bất kỳ. Cho biết doanh thu của mặt hàng nào bán được nhiều
nhất trong tháng năm đó.*/
create proc LayThongTinProBanNhieuNhat(@date1 datetime,@date2 datetime)
as
begin
select top 1 mh.*
	from hoadon hd,chitiet_hd ct,mathang mh
	where year(hd.ngayhd) = @date1 and month(hd.ngayhd) = @date2 and 
	mh.msmh = (select ct2.msmh
				from chitiet_hd ct2 
				group by ct2.msmh
				having sum(ct2.soluong) = (select max(maxSL) as topSL
											 from (
											select msmh,sum(soluong) maxSL
											from chitiet_hd
											group by msmh) as a))
end			

							
SELECT * FROM CHITIET_HD
SELECT * FROM HOADON
SELECT * FROM MATHANG
SELECT * FROM NHANVIEN
SELECT * FROM KHACHHANG
--
select * from mathang
where donvitinh in (N'Chiếc',N'Cái',N'Hộp')	