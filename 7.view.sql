--7.1)Cho biết doanh số bán của từng mặt hàng trong 6 tháng đầu năm 2011
select m.msmh MSMH,m.tenkh TenMH
from chitiet_hd c , mathang m,hoadon hd
where c.msmh = m.msmh and hd.mshd = c.mshd and hd.ngayhd between '2011-01-01' and '2011-06-30'
group by m.msmh,m.tenkh
--7.2)	Cho biết mặt hàng có doanh bán xuất lớn nhất. 
select m.msmh,m.tenkh
from chitiet_hd c , mathang m
where c.msmh = m.msmh
group by m.msmh,m.tenkh
having sum(c.thanhtien) = (
							select max(MaxDS)
							from (
									select sum(ct.thanhtien) as MaxDS
									from chitiet_hd as ct 
									group by ct.msmh
								) as a
							)
--7.3)Cho biết mặt hàng có tổng doanh thu lớn hơn 20.000.000. 
select m.msmh,m.tenkh, sum(c.thanhtien)
from chitiet_hd c,mathang m
where c.msmh = m.msmh
group by m.msmh,m.tenkh 
having sum(c.thanhtien) > 20000000
--7.4)Cho biết doanh thu của từng mặt hàng theo từng tháng. 
select m.msmh,m.tenkh,month(hd.ngayhd),sum(c.thanhtien)
from chitiet_hd c,mathang m,hoadon hd
where c.msmh = m.msmh and hd.mshd = c.mshd
group by m.msmh,m.tenkh ,month(hd.ngayhd)
--7.5)	Cho biết mặt hàng có doanh bán xuất lớn nhất. 
select m.msmh,m.tenkh
from chitiet_hd c , mathang m,hoadon hd
where c.msmh = m.msmh and hd.mshd = c.mshd and month(hd.ngayhd) = 11
group by m.msmh,m.tenkh 
having sum(c.thanhtien) = (
							select max(MaxDS)
							from (
									select sum(ct.thanhtien) as MaxDS
									from chitiet_hd as ct 
									group by ct.msmh
								) as a
							)
--7.6)Cho biết những khách hàng có tổng tiền mua hàng trên 10 triệu và địa chỉ ở TP.HCM.
create view "KH" as
select kh.mskh,kh.tenkh
from khachhang kh,hoadon hd
where kh.mskh = hd.mskh and hd.tongtien > 10000000 and kh.diachi = 'HCM'
group by kh.mskh,kh.tenkh
sp_helptext @objname = KH
--7.7)Cho biết danh sách mặt hàng có đơn giá từ 2 triệu đến 20 triệu. Có sử dụng WITH CHECK OPTION. Sau đó chèn thử một ản ghi vào view này. 
create view mh_2_20 as
select * 
from mathang
where dongia between 2000000 and 20000000 WITH CHECK OPTION;

insert into mh_2_20 values('MK1',N'Keyboard P/s 2','99','2000000',N'Chiếc')
------------------------------------------------
select * from mathang
select * from hoadon
select * from khachhang kh
select * from chitiet_hd